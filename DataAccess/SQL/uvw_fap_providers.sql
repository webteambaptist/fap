IF EXISTS(select * FROM sys.views where name = 'uvw_fap_providers')
	DROP VIEW dbo.uvw_fap_providers;
GO

/*
-------------------------------------------------------------------------------
Author       Jim Trefry
Created      11/06/2019
Project      FAP Intranet Site (ASP.NET Core 3 Rewrite)
Purpose      Provides the data source for the provider table in the FAP
             intranet site.

             Provides the same data as selPhysicianFromEcho, but without
             duplicates and includes comma delimited codes/ids for the 
             following child tables: 
               * Hospitals
               * Specialties
               * Languages
               * Addresses

             This view is used in place of the following stored procs used
             by the previous version of FAP:
			   * selPhysician
               * selPhysicianFromEcho
               * selPhysicianHospital,
               * selPhysicianSpecialty
               * selPhysicianLang
               * selPhysicianAddress
-------------------------------------------------------------------------------
Modification History
11/09/2019  Jim Trefry
     - Added the primary address to each provider result.
	 - Filtered delimited list of address Ids to exclude the primary address
	   and renamed column to "OtherAddressIds".
11/13/2019 Jim Trefry
     - Added IsAcceptingNewPatients and AgesTreatedText to the result.
11/14/2019 Jim Trefry
     - Added InsuranceGroupIds to the result.
-------------------------------------------------------------------------------
*/

CREATE VIEW [dbo].[uvw_fap_providers]
AS

WITH
--Since the lat/lng are provided by the Addresses table, and they are accurate to the street, we don't need the ZipCodes
--table to look up lat/lng by zip code. The comments are left in case it's needed in the future.
--ZipCodes AS (
--	-- The zipCode table is a more complete list of geocodes than zipData, and it includes the city name.
--	SELECT DISTINCT 
--		z.Zipcode, 
--		z.City,
--		z.State,
--		z.Lat,
--		z.Long
--	FROM [ProviderDatabase_Peak10_SProcs].[dbo].[zipcode] z
--),
ProviderHospitalMappings AS (
	SELECT p.Id ProviderId
	FROM Providers p
		LEFT JOIN Provider_Hospital_Mapping hm
			ON p.Id = hm.ProviderId
		LEFT JOIN Hospitals h
			ON hm.HospitalCode = h.Code
				AND h.IsDeleted != 1
				AND h.Code <> 'CLAY'
				AND h.Code <> '__RP'
	GROUP BY p.Id
	HAVING COUNT(*) > 0
),
PrimarySpecialtyMappings AS (
	--There is no constraint to prevent a provider from having multiple primary specialties
	--and there were several duplicates (bad data) upon checking.
	--This removes the duplicates by selecting the first primary specialty after sorting desc by last update.
	SELECT 
		ProviderId
		,SpecialtyCode 
	FROM (
		SELECT 
			ProviderId
			,SpecialtyCode
			,ROW_NUMBER() over (partition by ProviderId Order by LastUpdateDateTime desc) RowNumber
		FROM Provider_Specialty_Mapping sm
		WHERE sm.IsPrimary = 1
	) T
	WHERE RowNumber = 1
),
PrimaryAddresses AS (
	--There is no constraint to prevent a provider from having multiple addresses of the same type.
	--The original IntranetFAP web app selected an address with AddressType 3 randomly, and then
	--displayed it as the legal practice address.
	--This selects the first address with AddressType 3, sorted by state, city, address1 to provide
	--some level of predictability, but we still need a deterministic way to identify the primary address.
	--TODO: Find out if we can get a field to indicate the primary legal practice address.
	SELECT *
	FROM (
		SELECT 
			Id
			,ProviderId
			,RTRIM(LTRIM(Address1))		Address1
			,RTRIM(LTRIM(Address2))		Address2
			,RTRIM(LTRIM(Address3))		Address3
			,RTRIM(LTRIM(City))			City
			,RTRIM(LTRIM([State]))		[State]
			,LEFT(RTRIM(LTRIM(Zip)), 5)	ZipCode
			,RTRIM(LTRIM(Phone1))		Phone1
			,RTRIM(LTRIM(Fax))			Fax
			,Lat
			,Lng
			,ROW_NUMBER() over (partition by ProviderId Order by [State], City, Address1) RowNumber
		FROM Addresses a
		WHERE a.AddressType = 3 --Legal Practice
	) T
	WHERE RowNumber = 1
),
OtherAddresses AS (
	--Selects the Ids of all addresses for a provider (regardless of AddressType)
	--except for the primary address.
	--Note: There are cases where the primary address is identical to one or more of the "other" addresses
	--      For example, a provider could have a type 3 and type 4 address that are the same.
	--      There are also cases where a provider may have two identical type 4 addresses, but with a
	--      different phone or fax number.
	--      So, additional filtering must happen in the application layer to account for these edge cases.
	SELECT 
		a.Id,
		a.ProviderId
	FROM Addresses a
		LEFT JOIN PrimaryAddresses pa
			ON pa.Id = a.Id
	WHERE pa.Id IS NULL --Do not filter on AddressType
),
ProvidersBase AS (
	SELECT DISTINCT
		p.[Id]
		,p.[Guid]
		,p.[FirstName]
		,LEFT(p.[MiddleName], 1) as MiddleInit
		,p.[LastName]
		,p.[EchoPhysicianId]
		,p.[EchoDoctorNumber]
		,p.[GenderId]
		,p.[LegalPracticeName]
		,p.EchoSuffix
		,p.IsBPP
		,p.IsAcceptingNewPatients
		,psm.SpecialtyCode PrimarySpecialtyCode

		--Primary Legal Practice Location (somewhat random for now until data is fixed)
		,pa.Address1
		,pa.Address2
		,pa.Address3
		,pa.City
		,pa.State
		,pa.ZipCode
		,pa.Phone1
		,pa.Fax
		,pa.Lat
		,pa.Lng

		--Delimited Specialty Codes
		,STUFF((
			SELECT ',' + sm1.SpecialtyCode
			FROM Provider_Specialty_Mapping sm1
			WHERE sm1.ProviderId = p.Id
			ORDER BY LastUpdateDateTime
			FOR XML PATH('')), 1, LEN(','), ''
			) AS SpecialtyCodes

		--Delimited Language Codes
		,STUFF((
			SELECT ',' + lm.LanguageCode
			FROM Provider_Language_Mapping lm
			WHERE lm.ProviderId = p.Id
			FOR XML PATH('')), 1, LEN(','), '') AS LanguageCodes

		--Delimited Other Address Ids (not Primary Legal Practice Location)
		,STUFF((
			SELECT ',' + CONVERT(varchar, a.Id)
			FROM OtherAddresses a
			WHERE a.ProviderId = p.Id
			FOR XML PATH('')), 1, LEN(','), '') AS OtherAddressIds

		--Delimited Ages Treated Text
		,STUFF((
			SELECT ',' + ages.Text
			FROM (
				SELECT distinct am.ProviderId, a.Id, a.Text
				FROM Provider_AgesTreated_Mapping am
					JOIN AgesTreateds a
						ON a.Id = am.AgesTreatedId
			) ages
			WHERE ages.ProviderId = p.Id
			ORDER BY ages.Id --The id is the sort for the age text
			FOR XML PATH('')), 1, LEN(','), ''
			) AS AgesTreatedText

		--Delimited Insurance Group Ids
		,STUFF((
			SELECT ',' + CONVERT(varchar, ins.Id)
			FROM (
				SELECT DISTINCT pim.ProviderId, ig.Id
				FROM Insurance_Groups ig
					JOIN Provider_Insurance_Mapping pim
						ON pim.InsuranceId = ig.Id
			) ins
			WHERE ins.ProviderId = p.Id
			FOR XML PATH('')), 1, LEN(','), '') AS InsuranceGroupIds

	FROM  Providers p
		LEFT JOIN PrimarySpecialtyMappings psm
			ON p.Id = psm.ProviderId
		JOIN [ProviderDatabase_Echo].[dbo].[stfstatu] d
			ON LTRIM(RTRIM(d.dr_id)) = p.EchoPhysicianId
		JOIN [ProviderDatabase_Echo].[dbo].sta_tab st
			ON st.[cd] = d.[staff_cd]
		--provider must have an address of type 3 (see PrimaryAddress CTE for filter)
		JOIN PrimaryAddresses pa
			ON pa.ProviderId = p.Id

	WHERE st.cd in ('A', 'AH', 'ANC', 'AS', 'C', 'CN', 'P', 'PN') 
		--Note: RNA was replaced with CRNA
		--Also: TNEU is the correct code, the selPhysicianFromEcho proc uses an invalid code (TNUE).
		AND psm.SpecialtyCode NOT IN ('CRNA', 'PTR', 'TNEU')
		AND NOT (p.FirstName = 'Gregory' AND p.LastName ='Pennock')
),
ProvidersWithHospitals AS (
	SELECT p.*
		,STUFF((
			SELECT ',' + hm.HospitalCode
			FROM Provider_Hospital_Mapping hm
				JOIN Hospitals h
					on h.Code = hm.HospitalCode
			WHERE hm.ProviderId = p.Id
				AND h.IsDeleted != 1
				AND h.Code <> 'CLAY'
				AND h.Code <> '__RP'
			FOR XML PATH('')), 1, LEN(','), '') AS HospitalCodes

	FROM ProvidersBase p
		LEFT JOIN ProviderHospitalMappings phm
			ON phm.ProviderId = p.Id

	--IMPORTANT: Filter must be is opposite of ProviderId filter in the ProvidersWithEchoHospitals CTE.
	WHERE phm.ProviderId IS NOT NULL
),
ProvidersWithEchoHospitals AS (
	SELECT p.*
		,STUFF((
			SELECT ',' + s.fac_cd
			FROM [ProviderDatabase_Echo].[dbo].stfstatu s
				JOIN [ProviderDatabase_Echo].[dbo].drname dr on
					LTRIM(RTRIM(s.dr_id)) = LTRIM(RTRIM(dr.dr_id))
				JOIN Hospitals h 
					on h.Code = s.fac_cd
			Where LTRIM(RTRIM(s.dr_id)) = LTRIM(RTRIM(p.EchoPhysicianId))
				AND s.fac_cd <> 'CLAY'
				AND h.IsDeleted != 1
				AND h.Code <> '__RP'
			FOR XML PATH('')), 1, LEN(','), ''
		) AS HospitalCodes

	FROM ProvidersBase p
		LEFT JOIN ProviderHospitalMappings phm
			ON phm.ProviderId = p.Id
		
	--IMPORTANT: Filter must be is opposite of ProviderId filter in the ProvidersWithHospitals CTE.
	-- Minimizes the size of this very slow query due to lack of indexes.
	-- We only need to get hospitals from the Echo database if there are no direct mappings.
	WHERE phm.ProviderId IS NULL
)
SELECT * FROM ProvidersWithHospitals 
UNION ALL
SELECT * FROM ProvidersWithEchoHospitals
GO