﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using FAP.Models;

namespace FAP.DataAccess
{
    public partial class PhotosDbContext : DbContext
    {
        private readonly IConfiguration _configuration;

        public PhotosDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public PhotosDbContext(DbContextOptions<PhotosDbContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
        }

        public virtual DbSet<ProviderPhoto> ProviderPhotos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_configuration.GetConnectionString("EchoPictures"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProviderPhoto>(entity =>
            {
                entity.HasKey(e => e.DoctorNumber);

                entity.ToTable("Pictures");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
