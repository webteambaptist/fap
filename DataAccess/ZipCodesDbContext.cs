﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using FAP.Models;

namespace FAP.DataAccess
{
    public partial class ZipCodesDbContext : DbContext
    {
        private readonly IConfiguration _configuration;

        public ZipCodesDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ZipCodesDbContext(DbContextOptions<ZipCodesDbContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
        }

        public virtual DbSet<ZipCodeInfo> ZipCodes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_configuration.GetConnectionString("ProviderDatabase_Peak10_SProcs"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ZipCodeInfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Zipcode");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
