﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using FAP.Models;

namespace FAP.DataAccess
{
    public partial class FapDbContext : DbContext
    {
        private readonly IConfiguration _configuration;

        public FapDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public FapDbContext(DbContextOptions<FapDbContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
        }

        public virtual DbSet<Hospital> Hospitals { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Specialty> Specialties { get; set; }
      //  public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<InsuranceGroup> InsuranceGroups { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_configuration.GetConnectionString("ProviderDatabase_Peak10_Copy"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Hospital>(entity =>
            //{
            //    entity.HasKey(e => e.Code)
            //        .HasName("PK__Hospitals__09DE7BCC");

            //    entity.ToTable("Hospitals");

            //    entity.Property(e => e.Code).HasMaxLength(128);

            //    entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");
            //});

            //modelBuilder.Entity<Language>(entity =>
            //{
            //    entity.HasKey(e => e.Code)
            //        .HasName("PK__Languages__0DAF0CB0");

            //    entity.ToTable("Languages");

            //    entity.Property(e => e.Code).HasMaxLength(128);

            //    entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

            //    entity.Property(e => e.LastUpdateDateTime).HasColumnType("datetime");

            //    entity.Property(e => e.LastUpdateUser).HasMaxLength(50);
            //});

            //modelBuilder.Entity<Specialty>(entity =>
            //{
            //    entity.HasKey(e => e.Code);

            //    entity.ToTable("Specialties");

            //    entity.Property(e => e.Code).HasMaxLength(128);

            //    entity.Property(e => e.IsConstrained).HasDefaultValueSql("((0))");

            //    entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

            //    entity.Property(e => e.IsUrlOverride).HasDefaultValueSql("((0))");

            //    entity.Property(e => e.LastUpdateDateTime).HasColumnType("datetime");

            //    entity.Property(e => e.LastUpdateUser).HasMaxLength(50);
            //});

            //modelBuilder.Entity<Provider>(entity =>
            //{
            //    entity.HasNoKey();

            //    entity.ToView("uvw_fap_providers");

            //    entity.Property(e => e.GenderId).HasMaxLength(128);

            //    entity.Property(e => e.IsBpp).HasColumnName("IsBPP");

            //    entity.Property(e => e.PrimarySpecialtyCode).HasMaxLength(128);
            //});

            //modelBuilder.Entity<Address>(entity =>
            //{
            //    entity.ToView("Addresses");
            //});

            //modelBuilder.Entity<InsuranceGroup>(entity =>
            //{
            //    entity.ToView("Insurance_Groups");

            //    entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

            //    entity.Property(e => e.LastUpdateDateTime).HasColumnType("datetime");

            //    entity.Property(e => e.LastUpdateUser).HasMaxLength(50);
            //});

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
