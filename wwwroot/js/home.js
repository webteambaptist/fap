﻿/** Query parameters for provider list including paging, sorting and filtering. */
var query = {
    isPreferred: false, //changed on page load
    page: 1,
    pageCount: 10,
    sort: "name", //distance or name
    lastName: null,  //last name of provider
    isBPP: false,    //Baptist Physician Partner
    gender: null,    //M or F
    zipCode: null,   //search within zip code
    distance: null,  //search within x miles
    hospitals: [],  //hospital codes
    specialties: [], //specialty codes
    languages: []   //language codes
};

var startPage = 1;

$(document).ready(function () {

    if (window.location.href.indexOf("intranetfap") > -1 || window.location.href.indexOf("IntranetFAP") > -1) {
        mainUrl = '/IntranetFAP/';
    } else {
        mainUrl = '../';
    }

    var hospitals = sessionStorage.getItem("Hospitals");
    var specialties = sessionStorage.getItem("Specialties");
    var languages = sessionStorage.getItem("Languages");
    var isBpp = sessionStorage.getItem("isBPP");
    var genders = sessionStorage.getItem("Gender");
    var zipcode = sessionStorage.getItem("ZipCode");
    var distance = sessionStorage.getItem("Distance");

    initPageNumber(1);

    if (hospitals != null && specialties != null && languages != null && isBpp != null && genders != null && zipcode != null && distance != null) {
        var hospitalsSelected = JSON.parse(sessionStorage.getItem("Hospitals"));

        var specialtiesSelected = JSON.parse(sessionStorage.getItem("Specialties"));

        var languagesSelected = JSON.parse(sessionStorage.getItem("Languages"));

        var lastNameSearch = JSON.parse(sessionStorage.getItem("LastNameSearch"));

        var isBppSelected = JSON.parse(sessionStorage.getItem("isBPP"));
        var genderSelected = JSON.parse(sessionStorage.getItem("Gender"));
        var zipCodeSearch = JSON.parse(sessionStorage.getItem("ZipCode"));
        var distanceSelected = JSON.parse(sessionStorage.getItem("Distance"));
        var pageNumber = JSON.parse(sessionStorage.getItem("Page"));
        var pageAmount = JSON.parse(sessionStorage.getItem("PageCount"));
        var sortBy = JSON.parse(sessionStorage.getItem("Sort"));

        query = {
            isPreferred: false, //changed on page load
            page: pageNumber,
            pageCount: pageAmount,
            sort: sortBy, //distance or name
            lastName: lastNameSearch, //last name of provider
            isBPP: isBppSelected, //Baptist Physician Partner
            gender: genderSelected, //M or F
            zipCode: zipCodeSearch, //search within zip code
            distance: distanceSelected, //search within x miles
            hospitals: hospitalsSelected, //hospital codes
            specialties: specialtiesSelected, //specialty codes
            languages: languagesSelected //language codes
        };

        populateSearchFields();
        updatePagers(query.page);
       
        ProcessQuery(query);
    }
});

function resetFilters() {
    removePopulatedSearchFields();

    sessionStorage.clear();

    query = {
        isPreferred: false, //changed on page load
        page: 1,
        pageCount: 10,
        sort: "name", //distance or name
        lastName: null,  //last name of provider
        isBPP: false,    //Baptist Physician Partner
        gender: null,    //M or F
        zipCode: null,   //search within zip code
        distance: null,  //search within x miles
        hospitals: [],  //hospital codes
        specialties: [], //specialty codes
        languages: []   //language codes
    };

    startPage = 1;

    ProcessQuery(query);
}

function submitForm(resetPage) {
    if (resetPage) {
        startPage = 1;
        query.page = 1;
    }

    query.hospitals = aggregateCheckboxLists('cbHospital');

    if ($('#searchBySpecialty').prop('checked')) {
        query.specialties = aggregateCheckboxLists('cbSpecialty');
    } else {
        query.specialties = [];
    }
    if ($('#searchByLanguage').prop('checked')) {
        query.languages = aggregateCheckboxLists('cbLanguage');
    } else {
        query.languages = [];
    }
    query.lastName = document.getElementById("lastName").value.trim();

    var bpp = document.getElementById("isBPP");
    if (bpp) {
        query.isBPP = bpp.checked;
    }
    query.gender = document.getElementById("genderMale").checked ? "M" :
        document.getElementById("genderFemale").checked ? "F" : "";

    query.zipCode = document.getElementById("zipCode").value.trim();

    query.distance = parseInt(document.getElementById("searchDistance").value.trim() || undefined);

    ProcessQuery(query);
}

function ProcessQuery(query) {

    var modal = document.getElementById("myModal");
    modal.style.display = "block";

    sessionStorage.setItem("Hospitals", JSON.stringify(query.hospitals));
    sessionStorage.setItem("Specialties", JSON.stringify(query.specialties));
    sessionStorage.setItem("Languages", JSON.stringify(query.languages));
    sessionStorage.setItem("LastNameSearch", JSON.stringify(query.lastName));
    sessionStorage.setItem("isBPP", JSON.stringify(query.isBPP));
    sessionStorage.setItem("Gender", JSON.stringify(query.gender));
    sessionStorage.setItem("ZipCode", JSON.stringify(query.zipCode));
    sessionStorage.setItem("Distance", JSON.stringify(query.distance));
    sessionStorage.setItem("Page", JSON.stringify(query.page));
    sessionStorage.setItem("PageCount", JSON.stringify(query.pageCount));
    sessionStorage.setItem("Sort", JSON.stringify(query.sort));

    //get provider list with sorting, filtering and paging applied
    $.ajax({
        type: "POST",
        //url: '/IntranetFAP/Home/QueryProviders',
        url: mainUrl + 'Home/QueryProviders',
        data: JSON.stringify(query),
        contentType: "application/json",
        xhrFields: {
            withCredentials: true
        }
    }).done(function (html) {
        $("#providerList").html(html);

        setTimeout(function () {
            modal.style.display = "none";
        }, 450);

        updatePagers(query.page);
    });

    //prevent submit from posting back
    return false;
}

function populateSearchFields() {
    if (query.hospitals.length > 0) {
        selectOptions('cbHospital', query.hospitals);
    }

    if (query.specialties.length > 0) {
        document.getElementById("searchBySpecialty").checked = true;

        showHideSpecialties();
        selectOptions('cbSpecialty', query.specialties);
    }
    if (query.languages.length > 0) {
        document.getElementById("searchByLanguage").checked = true;

        showHideLanguages();
        selectOptions('cbLanguage', query.languages);
    }

    if (query.isBPP === true) {
        document.getElementById("isBPP").checked = true;
    }

    if (query.gender === "M") {
        document.getElementById("genderMale").checked = true;
    }
    else if (query.gender === "F") {
        document.getElementById("genderFemale").checked = true;
    }

    if (query.zipCode != null || query.zipCode !== "") {
        document.getElementById("zipCode").value = query.zipCode;
    }

    if (query.lastName != null || query.lastName !== "") {
        document.getElementById("lastName").value = query.lastName;
    }

    document.getElementById("resultsPerPageInHeader").value = query.pageCount;
    document.getElementById("resultsPerPageInFooter").value = query.pageCount;

    document.getElementById("searchDistance").value = query.distance;

    document.getElementById("sortInHeader").value = query.sort;
    document.getElementById("sortInFooter").value = query.sort;
}

function removePopulatedSearchFields() {
    if (query.hospitals.length > 0) {
        deSelectOptions('cbHospital', query.hospitals);
    }

    if (query.specialties.length > 0) {
        document.getElementById("searchBySpecialty").checked = false;

        showHideSpecialties();
        deSelectOptions('cbSpecialty', query.specialties);
    }

    if (query.languages.length > 0) {
        document.getElementById("searchByLanguage").checked = false;

        showHideLanguages();
        deSelectOptions('cbLanguage', query.languages);
    }

    if (query.gender === "M") {
        document.getElementById("genderMale").checked = false;
    }
    else if (query.gender === "F") {
        document.getElementById("genderFemale").checked = false;
    }

    if (query.zipCode != null || query.zipCode !== "") {
        document.getElementById("zipCode").value = null;
    }

    if (query.lastName != null || query.lastName !== "") {
        document.getElementById("lastName").value = null;
    }

    document.getElementById("isBPP").checked = false;
    document.getElementById("searchDistance").value = 5;

    document.getElementById("resultsPerPageInHeader").value = 10;
    document.getElementById("resultsPerPageInFooter").value = 10;

    document.getElementById("sortInHeader").value = "name";
    document.getElementById("sortInFooter").value = "name";
}

function selectOptions(checkBoxClass, selectedOptions) {
    values = [];
    var id;
    var cbs = document.getElementsByClassName(checkBoxClass);
    for (var i = 0; i < selectedOptions.length; i++) {
        Array.prototype.forEach.call(cbs, function (i) {
            var option = i.value;

            var ua = window.navigator.userAgent;
            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                var doesExist = checkResultSet(selectedOptions, option);
                if (doesExist === true) {
                    id = i.id;
                    document.getElementById(id).checked = true;
                }
            }
            else {
                if (selectedOptions.includes(option)) {
                    id = i.id;
                    document.getElementById(id).checked = true;
                }
            }
            i++;
        });
    }
    return values;
}

function deSelectOptions(checkBoxClass, selectedOptions) {
    values = [];
    var id;
    var cbs = document.getElementsByClassName(checkBoxClass);
    for (var i = 0; i < selectedOptions.length; i++) {
        Array.prototype.forEach.call(cbs, function (i) {
            var option = i.value;

            var ua = window.navigator.userAgent;
            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                var doesExist = checkResultSet(selectedOptions, option);
                if (doesExist === true) {
                    id = i.id;
                    document.getElementById(id).checked = false;
                }
            }
            else {
                if (selectedOptions.includes(option)) {
                    id = i.id;
                    document.getElementById(id).checked = false;
                }
            }
            i++;
        });
    }
    return values;
}

function checkResultSet(container, value) {
    var returnValue = false;
    var pos = container.indexOf(value);
    if (pos >= 0) {
        returnValue = true;
    }
    return returnValue;
}

function aggregateCheckboxLists(checkBoxClass) {
    values = [];
    var cbs = document.getElementsByClassName(checkBoxClass);
    Array.prototype.forEach.call(cbs, function (cb) {
        if (cb.checked) {
            values.push(cb.value);
        }
    });
    return values;
}

function showHideSpecialties() {
    var cb = document.getElementById("searchBySpecialty");
    var pnl = document.getElementById("specialtyPanel");
    pnl.style.display = cb.checked ? 'block' : 'none';
}

function toggleAllSpecialties(isChecked) {
    var cbs = document.getElementsByClassName("cbSpecialty");
    Array.prototype.forEach.call(cbs, function (cb) {
        cb.checked = isChecked;
    });
}

function showHideLanguages() {
    var cb = document.getElementById("searchByLanguage");
    var pnl = document.getElementById("languagePanel");
    pnl.style.display = cb.checked ? 'block' : 'none';
}

function toggleAllLanguages(isChecked) {
    var cbs = document.getElementsByClassName("cbLanguage");
    Array.prototype.forEach.call(cbs, function (cb) {
        cb.checked = isChecked;
    });
}

function changeSort() {
    query.sort = event.target.value;

    sortInHeader = document.getElementById("sortInHeader");
    sortInFooter = document.getElementById("sortInFooter");

    sortInHeader.value = query.sort;
    sortInFooter.value = query.sort;

    sessionStorage.setItem("Sort", JSON.stringify(query.page));
    submitForm(false);
}

function initPageNumber(pageNumber) {
    var page = parseInt(pageNumber);
    if (page && page >= 1) {
        query.page = page;
        startPage = page;
        updatePagers(page);
        //updateProfileLinks(page);
    }
}

function changePageNumber(pageNumber) {
    if (pageNumber <= 0) {
        pageNumber = 1;
    }

    //update the page in the query
    var newPageNumber = parseInt(pageNumber);
    if (query.page !== newPageNumber) {
        query.page = newPageNumber;

        sessionStorage.setItem("Page", JSON.stringify(query.page));
        //get data for the selected page
        submitForm(false);
    }
}

function changePageCount(pageCount) {
    query.pageCount = parseInt(pageCount);

    //Reset to page 1, or the current page may not have any items if the page count increased.
    query.page = 1;

    //sync the page count in the header and footer
    var header = document.getElementById("resultsPerPageInHeader");
    if (header.value !== pageCount) {
        header.value = pageCount;
    }
    var footer = document.getElementById("resultsPerPageInFooter");
    if (footer.value !== pageCount) {
        footer.value = pageCount;
    }

    sessionStorage.setItem("PageCount", JSON.stringify(query.pageCount));

    submitForm(true);
}

function moveToPreviousPage() {
    startPage--;
    changePageNumber(query.page - 1);

    var providerList = document.getElementById("searchResultsList");
    providerList.scrollIntoView();
}

function moveToNextPage() {
    startPage++;
    changePageNumber(query.page + 1);

    var providerList = document.getElementById("searchResultsList");
    providerList.scrollIntoView();
}

function updatePagers(pageNumber) {
    if (pageNumber <= 0) {
        pageNumber = 1;
    }

    //get the total count of providers returned from the query
    var totalCount = parseInt($('#totalProviderCount')[0].value);

    if (totalCount === 0) {
        var providerList = $('#providerList');
        providerList.empty();
        providerList.html('<div style="margin:10px">There are no results</div>');
    }
    //update pagers
    var pagerLocations = ['InHeader', 'InFooter'];
    for (var i = 0; i < pagerLocations.length; i++) {

        //Get DOM references
        var pager = $('#pager' + pagerLocations[i]);
        var pagerPreviousLink = $('#pagerPrevious' + pagerLocations[i]);
        var pagerNextLink = $('#pagerNext' + pagerLocations[i]);
        var pagerSummary = $('#pagerSummary' + pagerLocations[i]);

        //update pager summary
        if (totalCount === 0) {
            pagerSummary.text('0 out of 0');
        } else {
            var startIndex = 1;
            if (pageNumber > 1) {
                startIndex = (pageNumber - 1) * query.pageCount + 1;
            }
            var lastIndex = pageNumber * query.pageCount;
            if (totalCount < lastIndex) {
                lastIndex = totalCount;
            }
            pagerSummary.text(startIndex + ' - ' + lastIndex + ' of ' + totalCount);
        }

        //recalculate start page
        if (pageNumber > 10) {
            startPage = pageNumber - 9;
        }
        //remove all page links, and insert new page links as appropriate
        var maxPage = startPage + 9;
        var lastPage = startPage; //initial value
        pager.empty();
        for (var page = startPage; page <= maxPage; page++) {

            //dont' show page numbers that don't have records
            var firstIndexOnPage = 1;
            if (page > 1) {
                firstIndexOnPage = (page - 1) * query.pageCount + 1;
            }
            if (firstIndexOnPage > totalCount) {
                break;
            }

            //create the page number link
            var td = $(document.createElement('td'));
            td.html('<a href="javascript:changePageNumber(' + page + ')"' + (page == pageNumber ? 'class="currentPage"' : '') + '>' + page + '</a>');
            pager.append(td);

            lastPage = page;
        }

        //enable disable previous/next buttons
        if (totalCount >= 0 && startPage <= 1) {
            pagerPreviousLink.hide();
        } else {
            pagerPreviousLink.show();
        }
        var nextPageHasItems = (totalCount - (lastPage * query.pageCount)) > 0;

        if (nextPageHasItems) {
            pagerNextLink.show();
        } else {
            pagerNextLink.hide();
        }
    }
}
