﻿using AutoMapper;
using FAP.DTOs;
using FAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;

namespace FAP.Services
{
    public class MapperService : IMapperService
    {
        private readonly IMapper _autoMapper;
        private readonly IConfiguration _configuration;

        public MapperService(IMapper autoMapper, IConfiguration configuration)
        {
            _autoMapper = autoMapper;
            _configuration = configuration;
        }

        public ProviderDTO Map(FinalFAPProvider e, List<HospitalDTO> hospitals, List<SpecialtyDTO> specialties, List<LanguageDTO> languages,
            List<AddressDTO> addresses)
        {
            var dto = new ProviderDTO
            {
                Id = e._Fapproviders.Id,
                Guid = e._Fapproviders.Guid,
                FormattedCommonName = e._Fapproviders.FormattedCommonName,
                EchoDoctorNumber = e._Fapproviders.EchoDoctorNumber,
                GenderId = e._Fapproviders.GenderId,
                EchoSuffix = e._Fapproviders.EchoSuffix,
                IsAcceptingNewPatients = e._Fapproviders.IsAcceptingNewPatients != null && (bool)e._Fapproviders.IsAcceptingNewPatients,
                FormattedJobTitle = e._Fapproviders.FormattedJobTitle,
                LegalPracticeName = e._Fapproviders.LegalPracticeName,
                IsBPP = e._Fapproviders.IsBpp,
                FirstName = e._Fapproviders.FirstName,
                MiddleName = e._Fapproviders.MiddleName,
                LastName = e._Fapproviders.LastName
            };
            if (e._Fapproviders.DoctorImage != "")
            {
                dto.DoctorImage = e._Fapproviders.DoctorImage;
            }
            #region Hospital Mapping
            if (!string.IsNullOrEmpty(e._Fapproviders.HospitalCodes))
            {
                var resultHosptial = e._Fapproviders.HospitalCodes.Split(',');

                foreach (var hospital in resultHosptial)
                {
                    var h = hospitals.FirstOrDefault(x => x.Code.Equals(hospital, StringComparison.OrdinalIgnoreCase));
                    if (h != null)
                    {
                        dto.Hospitals.Add(h);
                    }
                }
            }
            #endregion
            #region SpecialtyCodes
            if (!string.IsNullOrEmpty(e._Fapproviders.SpecialtyCodes))
            {
                var resultSpecialty = e._Fapproviders.SpecialtyCodes.Split(',');
                foreach (var specialty in resultSpecialty)
                {
                    var s = specialties.FirstOrDefault(x => x.Code.Equals(specialty, StringComparison.OrdinalIgnoreCase));
                    if (s != null)
                    {
                        dto.Specialties.Add(s);
                    }
                }
            }
            #endregion
            #region Primary Specialty

            //Map primary specialty

            if (!string.IsNullOrEmpty(e._Fapproviders.FormattedJobTitle))
            {
                var primarySpecialty = e._Fapproviders.FormattedJobTitle;
                if (primarySpecialty != null)
                {
                    dto.FormattedJobTitle = primarySpecialty;
                }
            }
            #endregion
            #region LanguageCodes
            if (!string.IsNullOrEmpty(e._Fapproviders.LanguageCodes))
            {
                var resultLanguage = e._Fapproviders.LanguageCodes.Split(',');
                foreach (var language in resultLanguage)
                {
                    var l = languages.FirstOrDefault(x => x.Code.Equals(language, StringComparison.OrdinalIgnoreCase));
                    if (l != null)
                    {
                        dto.Languages.Add(l);
                    }
                }
            }
            #endregion
            #region AgesTreated
            if (!string.IsNullOrEmpty(e._Fapproviders.AgesTreatedText))
            {
                var resultAgesTreated = e._Fapproviders.AgesTreatedText.Split(',');
                foreach (var agesTreated in resultAgesTreated)
                {
                    dto.AgesTreated.Add(agesTreated);
                }
            }
            #endregion
            ////Add the primary location to the Addresses child collection.

            //Save Primary Location
            dto.Address1 = e.Address1;
            dto.Address2 = e.Address2;
            dto.Address3 = e.Address3;
            dto.City = e.City;
            dto.State = e.State;
            dto.ZipCode = e.Zip;
            dto.Phone1 = e.MainPhone;
            dto.Fax = e.Fax;
            dto.MainPhone = e.MainPhone;

            dto.Lat = e.Lat.ToString() == ""
                ? Convert.ToDouble("0.0")
                : Convert.ToDouble(e.Lat);

            dto.Lng = e.Lng.ToString() == ""
                ? Convert.ToDouble("0.0")
                : Convert.ToDouble(e.Lng);

            if (string.IsNullOrEmpty(e._Fapproviders.OtherAddressIds)) 
                return dto;
            {
                var resultLocation = e._Fapproviders.OtherAddressIds.Split(',');
                foreach (var location in resultLocation)
                {
                    var a = addresses.FirstOrDefault(x => x.Id == Convert.ToInt32(location));
                    if (a != null)
                    {
                        dto.Addresses.Add(a);
                    }
                }
            }
            //Some providers do not have addresses filled in so we have to fill it in or it won't be included in the  main list
            if (!string.IsNullOrEmpty(dto.Address1) || dto.Addresses.Count <= 0) return dto;
            var address = dto.Addresses.First();
            dto.Address1 = address.Address1;
            dto.Address2 = address.Address2;
            dto.Address3 = address.Address3;
            dto.City = address.City;
            dto.State = address.State;
            dto.ZipCode = address.Zip;
            dto.Phone1 = address.MainPhone;
            dto.Fax = address.Fax;
            dto.MainPhone = address.MainPhone;

            dto.Lat = address.Lat.ToString() == ""
                ? Convert.ToDouble("0.0")
                : Convert.ToDouble(e.Lat);

            dto.Lng = address.Lng.ToString() == ""
                ? Convert.ToDouble("0.0")
                : Convert.ToDouble(e.Lng);
            return dto;
        }

        public HospitalDTO Map(Hospital h)
        {
            //Note: Logic is carried over from the (pre .NET Core) IntranetFAP project.
            var text = h.HospitalName.Trim();
            var dto = new HospitalDTO() { Code = h.HospitalCode, Text = text };

            string abbrev;
            if (text != "" && text.Contains("Baptist Medical Center"))
            {
                var i = 22;
                var l = text.Length - i;
                abbrev = text.Substring(i, l).Trim();
            }
            else if (text != "" && text.Contains("Wolfson"))
            {
                abbrev = "Wolfson";
            }
            else if (text != "" && text.Contains("Baptist Clay"))
            {
                abbrev = "Clay";
            }
            else
            {
                abbrev = text;
            }

            dto.Abbrev = abbrev;

            return dto;
        }

        public SpecialtyDTO Map(Specialty e)
        {
            var dto = _autoMapper.Map<SpecialtyDTO>(e);
            return dto;
        }

        public LanguageDTO Map(Language e)
        {
            var dto = _autoMapper.Map<LanguageDTO>(e);
            return dto;
        }

        public AddressDTO Map(Address e)
        {
            var dto = _autoMapper.Map<AddressDTO>(e);
            return dto;
        }
    }
}
