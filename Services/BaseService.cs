﻿using System;
using System.Net;

namespace FAP.Services
{
    public class BaseService
    {
        public static HttpWebResponse GetDataFromApi(string strApiMethod, string strApiInput = "", string strMethod = "GET")
        {
            HttpWebResponse webResponse = null;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(strApiMethod);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = strMethod;
                if (strMethod.ToLower().Equals("post"))
                    httpWebRequest.ContentLength = 0;
                httpWebRequest.Timeout = 200000000;
                if (!string.IsNullOrEmpty(strApiInput))
                {
                    if (strApiInput.Contains('^'))
                    {
                        foreach (var strHeaders in strApiInput.Split('^'))
                        {
                            if (!strHeaders.Contains('|'))
                                continue;

                            var strHeader = strHeaders.Split('|');
                            httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
                        }
                    }
                    else if (strApiInput.Contains('|'))
                    {
                        var strHeader = strApiInput.Split('|');
                        httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
                    }
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (Exception)
            {
                // ignored
            }

            return webResponse;
        }

    }
}
