﻿using System;
using FAP.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using NLog;

namespace FAP.Services
{
    public class DataService : BaseService, IDataService
    {
        private readonly IConfiguration _configuration;
        private readonly string _serviceUri;

        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public List<FinalFAPProvider> allProviders;

        public DataService(IConfiguration configuration)
        {
            _configuration = configuration;
            _serviceUri = _configuration["AppSettings:ServiceUri"];

            var logConfig = new NLog.Config.LoggingConfiguration();

            var orFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs/IntranetFAPDataLog-{DateTime.Now:MM-dd-yyyy}.log" };
            logConfig.AddRule(LogLevel.Info, LogLevel.Fatal, orFile);
            LogManager.Configuration = logConfig;

            //allProviders = GetAllProviders();
        }

        public List<FinalFAPProvider> GetAllProviders()
        {
            var providers = new List<FinalFAPProvider>();
            var getProviders = _configuration["AppSettings:GetProviders"];
            var results = GetDataFromApi(_serviceUri + getProviders);

            if (results == null)
            {
                Logger.Error("There was an issue retrieving providers from URL: " + _serviceUri + getProviders + " :: Providers results returned null");
                return providers;
            }

            using (var reader = new StreamReader(results.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                var objText = reader.ReadToEnd();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                providers = (List<FinalFAPProvider>)JsonConvert.DeserializeObject(objText, typeof(List<FinalFAPProvider>), settings);
            }

            return providers;
        }

        public List<Hospital> GetAllHospitals()
        {
            var hospitals = new List<Hospital>();
            var getHospitals = _configuration["AppSettings:GetHospitals"];
            var results = GetDataFromApi(_serviceUri + getHospitals);

            if (results == null)
            {
                Logger.Error("There was an issue retrieving Hospitals from URL: " + _serviceUri + getHospitals + " :: Hospitals results returned null");
                return hospitals;
            }

            using (var reader = new StreamReader(results.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                var objText = reader.ReadToEnd();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                hospitals = (List<Hospital>)JsonConvert.DeserializeObject(objText, typeof(List<Hospital>), settings);

                hospitals = hospitals.OrderBy(h => h.HospitalName)
                    .ToList();
            }
            return hospitals;
        }

        public List<Specialty> GetAllSpecialties()
        {
            var excludedCodes = new[] { "CRNA", "PTR", "TNEU", "RNA" };

            var specialties = new List<Specialty>();
            var getSpecialties = _configuration["AppSettings:GetSpecialties"];
            var results = GetDataFromApi(_serviceUri + getSpecialties);

            if (results == null)
            {
                Logger.Error("There was an issue retrieving Specialties from URL: " + _serviceUri + getSpecialties + " :: Specialties results returned null");
                return specialties;
            }

            using (var reader = new StreamReader(results.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                var objText = reader.ReadToEnd();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                specialties = (List<Specialty>)JsonConvert.DeserializeObject(objText, typeof(List<Specialty>), settings);

                specialties = specialties.Where( s=> !excludedCodes.Contains(s.Code.Trim())).ToList();
            }
            return specialties;
        }

        public List<Language> GetAllLanguages()
        {
            var languages = new List<Language>();
            var getLanguages = _configuration["AppSettings:GetLanguages"];
            var results = GetDataFromApi(_serviceUri + getLanguages);

            if (results == null)
            {
                Logger.Error("There was an issue retrieving Languages from URL: " + _serviceUri + getLanguages + " :: Languages results returned null");
                return languages;
            }

            using (var reader = new StreamReader(results.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                var objText = reader.ReadToEnd();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                languages = (List<Language>)JsonConvert.DeserializeObject(objText, typeof(List<Language>), settings);
            }
            return languages;
        }

        public List<Address> GetAllAddresses()
        {
            var addresses = new List<Address>();
            var getLocations = _configuration["AppSettings:GetLocations"];
            var results = GetDataFromApi(_serviceUri + getLocations);

            if (results == null)
            {
                Logger.Error("There was an issue retrieving Addresses from URL: " + _serviceUri + getLocations + " :: Addresses results returned null");
                return addresses;
            }

            using (var reader = new StreamReader(results.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                var objText = reader.ReadToEnd();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                var locations = (List<Address>) JsonConvert.DeserializeObject(objText, typeof(List<Address>),
                    settings);

                foreach (var location in locations)
                {
                    var a = new Address
                    {
                        Id = location.Id,
                        ProviderGuid = location.ProviderGuid,
                        LocationGuid = location.LocationGuid,
                        Name = location.Name,
                        Address1 = location.Address1,
                        Address2 = location.Address2,
                        Address3 = location.Address3,
                        City = location.City,
                        State = location.State,
                        Zip = location.Zip,
                        Phone = location.Phone,
                        Fax = location.Fax,
                        IsAcceptingEappointments = location.IsAcceptingEappointments,
                        IsDefaultLocation = location.IsDefaultLocation,
                        SequenceId = location.SequenceId,
                        Lat = location.Lat,
                        Lng = location.Lng,
                        MainPhone = location.MainPhone,
                        AdmissionsPhone = location.AdmissionsPhone
                    };
                    addresses.Add(a);
                }
                //addresses.AddRange(from providerAddress in locations
                //                   from locations in providerAddress.Locations
                //                   select new Address
                //                   {
                //                       //ProviderId = providerAddress.Id,
                //                       Address1 = locations.Address1,
                //                       Address2 = locations.Address2,
                //                       Address3 = locations.Address3,
                //                       City = locations.City,
                //                       State = locations.State,
                //                       Zip = locations.Zip,
                //                       Phone1 = locations.mainPhone,
                //                       Fax = locations.Fax,
                //                       Lat = locations.Lat.ToString() == "" ? Convert.ToDouble("0.0") : Convert.ToDouble(locations.Lat),
                //                       Lng = locations.Lng.ToString() == ""
                //                           ? Convert.ToDouble("0.0")
                //                           : Convert.ToDouble(locations.Lng)
                //                   });
            }
            return addresses;
        }

        public ZipCodeInfo GetZipCodeInfo(double zipCodeNumeric)
        {
            //var test = allProviders;
            // var providers = GetAllProviders();
            var zip = zipCodeNumeric.ToString(CultureInfo.InvariantCulture);
            var getLocationsZip = _configuration["AppSettings:GetLocationZip"];
            var test = GetDataFromApi(_serviceUri + getLocationsZip, "zip|" + zipCodeNumeric);

            using (var reader = new StreamReader(test.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                var objText = reader.ReadToEnd();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                var info = (Locations) JsonConvert.DeserializeObject(objText, typeof(Locations), settings);

                var zipCodeInfo = new ZipCodeInfo();
                var results = new Locations();

                if (info != null)
                {
                    var newResult = new Locations
                    {
                        Zip = info.Zip,
                        City = info.City,
                        State = info.State,
                        Lat = (double) info.Lat,
                        Lng = (double) info.Lng
                    };
                    results = newResult;
                }

                if (results.Zip == null)
                    return zipCodeInfo;

                if (results.Zip.Length >= 5)
                {
                    var zipcode = results.Zip.Substring(0, 5);
                    zipCodeInfo.ZipCode = Convert.ToDouble(zipcode);
                }
                else
                {
                    zipCodeInfo.ZipCode = Convert.ToDouble(results.Zip);
                }

                zipCodeInfo.City = results.City;
                zipCodeInfo.State = results.State;
                zipCodeInfo.Lat = Convert.ToDouble(results.Lat.ToString().StartsWith("-")
                    ? results.Lat.ToString().Substring(0, 6)
                    : results.Lat.ToString().Substring(0, 5));
                zipCodeInfo.Long = Convert.ToDouble(results.Lng.ToString().StartsWith("-")
                    ? results.Lng.ToString().Substring(0, 6)
                    : results.Lng.ToString().Substring(0, 5));

                return zipCodeInfo;
            }


            //var providers = _cache.GetProviders(false);
                //var zip = zipCodeNumeric.ToString(CultureInfo.InvariantCulture);
                //var zipCodeInfo = new ZipCodeInfo();
                //var results = new Locations();

                //var info = providers.Where(x => x.ZipCode)
                // var info = providers.FirstOrDefault(p => p.Zip.Contains(zip));

                //if (info != null)
                //{
                //    var newResult = new Locations
                //    {
                //        Zip = info.Zip,
                //        City = info.City,
                //        State = info.State,
                //        Lat = (double)info.Lat,
                //        Lng = (double)info.Lng
                //    };
                //    results = newResult;
                //    //break;
                //}
                //foreach (var provider in providers)
                //{

                //if (provider.Zip == null)
                //    continue;

                //var info = provider.Zip.Contains(zip);

                //var newResult = new Locations()
                //{
                //    Zip = provider.Zip,
                //    City = provider.City,
                //    State = provider.State,
                //    Lat = (double)provider.Lat,
                //    Lng = (double)provider.Lng
                //};
                //results = newResult;
                //break;

                //    if (provider.Locations.Count <= 0)
                //        continue;

                // var info = provider.Locations.FirstOrDefault(p => p.Zip.Contains(zip));

                //    if (info == null)
                //    {
                //        var locationMappingInfo = provider.Locations.FirstOrDefault(p => p.Zip.Contains(zip));
                //        if (locationMappingInfo == null)
                //        {
                //            continue;
                //        }

                //        var newResult = new Locations()
                //        {
                //            Zip = locationMappingInfo.Zip,
                //            City = locationMappingInfo.City,
                //            State = locationMappingInfo.State,
                //            Lat = locationMappingInfo.Lat,
                //            Lng = locationMappingInfo.Lng
                //        };
                //        results = newResult;
                //        break;
                //    }

                //    results = info;
                //    break;
                // }
            }
    }
}
