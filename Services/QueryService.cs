﻿using FAP.DTOs;
using NLog;
using System;
using System.Linq;

namespace FAP.Services
{
    public class QueryService : IQueryService
    {
        private readonly ICacheService _cache;
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public QueryService(ICacheService cache)
        {
            _cache = cache;
            var logConfig = new NLog.Config.LoggingConfiguration();

            var orFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs/IntranetFAPQueryLog-{DateTime.Now:MM-dd-yyyy}.log" };
            logConfig.AddRule(LogLevel.Info, LogLevel.Fatal, orFile);
            LogManager.Configuration = logConfig;
        }

        /// <summary>
        /// Applies filters, sorting and paging to the cached list of providers
        /// based on the provided <see cref="SearchQueryDTO"/>.
        /// </summary>
        /// <param name="queryDTO"></param>
        /// 
        public ProviderQueryResultDTO QueryProviders(SearchQueryDTO queryDTO)
        {
            var result = new ProviderQueryResultDTO() { IsPreferred = queryDTO.IsPreferred};

            var providerDTOs = _cache.GetProviders(true);


            //Calculate the provider's distance from the search zip code for every provider.
            //This must happen before sorting since "distance" is one of the sort options.
            var searchAreaZipInfo = _cache.GetZipCodeInfo(queryDTO.ZipCode);
            foreach (var p in providerDTOs)
            {
                try
                {
                    var lat = p.Lat;
                    var lng = p.Lng;

                    var distance = GetGeoDistance(lat, lng, searchAreaZipInfo.Lat, searchAreaZipInfo.Long);

                    p.SearchDistance = Math.Round(distance, 2);
                    p.SearchFromCity = searchAreaZipInfo.City;
                    p.SearchFromState = searchAreaZipInfo.State;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Logger.Error("There was an issue retrieving Lat/Lng info for provider:: " + p.FirstName + " " + p.LastName);
                }
            }

            ////Apply filters
            var query = providerDTOs.AsQueryable();
            ////Distance
            if (!searchAreaZipInfo.IsDefault && queryDTO.Distance != null)
            {
                query = query.Where(p => p.SearchDistance <= queryDTO.Distance);

                foreach (var p in providerDTOs)
                {
                    p.SearchFromCity = "searched Zip Code";
                    p.SearchFromState = "";
                }
            }
            ////IsBPP
            if (queryDTO.IsBPP)
            {
                query = query.Where(p => p.IsBPP == true);
            }
            ////LastName
            var lastName = (queryDTO.LastName ?? "").Trim().ToUpper();
            if (lastName.Length > 0)
            {
                query = query.Where(p => (p.FormattedCommonName ?? "").ToUpper().Contains(lastName));
            }
            ////Gender
            var gender = (queryDTO.Gender ?? "").Trim().ToUpper();
            if (gender.Length > 0 && (gender == "M" || gender == "F"))
            {
                query = query.Where(p => (p.GenderId ?? "").ToUpper() == gender);
            }
            ////Hospitals
            if (queryDTO.Hospitals != null && queryDTO.Hospitals.Length > 0)
            {
                query = query.Where(p => p.Hospitals.Any(h => queryDTO.Hospitals.Contains(h.Code)));
            }
            ////Specialties
            if (queryDTO.Specialties != null && queryDTO.Specialties.Length > 0)
            {
                query = query.Where(p => p.Specialties.Any(s => queryDTO.Specialties.Contains(s.Text)));
            }
            ////Languages
            if (queryDTO.Languages != null && queryDTO.Languages.Length > 0)
            {
                query = query.Where(p => p.Languages.Any(l => queryDTO.Languages.Contains(l.Text)));
            }

            ////Apply sort
            if (queryDTO.Sort != null)
            {
                query = queryDTO.Sort == "name" ? query.OrderBy(p => p.LastName).ThenBy(p => p.FirstName).ThenBy(p => p.MiddleName) : query.OrderBy(p => p.SearchDistance).ThenBy(p => p.LastName).ThenBy(p => p.FirstName).ThenBy(p => p.MiddleName);
            }

            //The search index is displayed in the UI, and changes depending
            //on the sort and filter, so it must be set after sorting and filtering
            //but before paging.
            var searchIndex = 1;
            providerDTOs = query.ToList();
            providerDTOs.ForEach(p => p.SearchIndex = searchIndex++);
            result.TotalCount = providerDTOs.Count;

            //Apply paging
            var page = Math.Max((queryDTO?.Page ?? 1) - 1, 0);
            var pageCount = Math.Max((queryDTO?.PageCount ?? 10), 5);
            providerDTOs = providerDTOs
                .Skip(page * pageCount)
                .Take(pageCount)
                .ToList();

            result.Providers = providerDTOs;

            return result;
        }

        protected double GetGeoDistance(double fromLatitude, double fromLongitude, double toLatitude, double toLongitude)
        {
            const double earthsRadius = 3956.08710710349;
            const int degrees = 180;

            var latRadians = (fromLatitude / degrees) * Math.PI;
            var longRadians = (fromLongitude / degrees) * Math.PI;

            var latRadians2 = (toLatitude / degrees) * Math.PI;
            var longRadians2 = (toLongitude / degrees) * Math.PI;

            var distance = (earthsRadius * 2) * Math.Asin(
                Math.Sqrt(
                    Math.Pow(
                        Math.Sin((latRadians - latRadians2) / 2), 2) +
                    Math.Cos(latRadians) * Math.Cos(latRadians2) *
                    Math.Pow(
                        Math.Sin((longRadians - longRadians2) / 2), 2)));

            return Math.Round(distance, 2);
        }
    }
}
