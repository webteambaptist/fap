﻿using FAP.DTOs;
using FAP.Models;
using System.Collections.Generic;

namespace FAP.Services
{
    public interface ICacheService
    {
        void Clear();

        List<ProviderDTO> GetProviders(bool cloneList); 

        ProviderDTO GetProvider(int providerId);

        List<HospitalDTO> GetHospitals();

        List<SpecialtyDTO> GetSpecialties();

        List<LanguageDTO> GetLanguages();

        List<AddressDTO> GetAddresses();

        ZipCodeInfo GetZipCodeInfo(string zipCode);
    }
}