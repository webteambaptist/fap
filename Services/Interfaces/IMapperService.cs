﻿using FAP.DTOs;
using FAP.Models;
using System.Collections.Generic;

namespace FAP.Services
{
    public interface IMapperService
    {
        ProviderDTO Map(FinalFAPProvider e, List<HospitalDTO> hospitals, List<SpecialtyDTO> specialties, List<LanguageDTO> languages, List<AddressDTO> addresses);

        HospitalDTO Map(Hospital h);

        SpecialtyDTO Map(Specialty e);
        
        LanguageDTO Map(Language e);
        
        AddressDTO Map(Address e);
    }
}