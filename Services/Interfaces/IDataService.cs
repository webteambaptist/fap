﻿using FAP.Models;
using System.Collections.Generic;

namespace FAP.Services
{
    public interface IDataService
    {
        List<FinalFAPProvider> GetAllProviders();

        List<Hospital> GetAllHospitals();
        
        List<Specialty> GetAllSpecialties();

        List<Language> GetAllLanguages();

        List<Address> GetAllAddresses();

        ZipCodeInfo GetZipCodeInfo(double zipCodeNumeric);
    }
}