﻿using FAP.DTOs;

namespace FAP.Services
{
    public interface IQueryService
    {
        ProviderQueryResultDTO QueryProviders(SearchQueryDTO query);
    }
}