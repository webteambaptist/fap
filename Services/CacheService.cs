﻿using FAP.Config;
using FAP.DTOs;
using FAP.Logging;
using FAP.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Threading;

namespace FAP.Services
{
    public class CacheService : ICacheService
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly IAppSettings _appSettings;
        private readonly IMemoryCache _cache;
        private readonly IDataService _dataService;
        private readonly IMapperService _mapper;
        private DateTime? _expirationTime;
        private static CancellationTokenSource _clearCacheToken = new CancellationTokenSource();

        public CacheService(IAppSettings appSettings, IMemoryCache cache, IDataService dataService, IMapperService mapper)
        {
            _appSettings = appSettings;
            _cache = cache;
            _dataService = dataService;
            _mapper = mapper;

            var logConfig = new NLog.Config.LoggingConfiguration();

            var orFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs/IntranetFAPCacheLog-{DateTime.Now:MM-dd-yyyy}.log" };
            logConfig.AddRule(LogLevel.Info, LogLevel.Fatal, orFile);
            LogManager.Configuration = logConfig;
        }

        private MemoryCacheEntryOptions BuildCacheOptions()
        {
            //ensures that all cached items expire at exactly the same time
            if (_expirationTime is null || DateTime.Now > _expirationTime)
            {
                _expirationTime = DateTime.Now.AddHours(_appSettings.CacheTimeInHours ?? 12);
            }

            var options = new MemoryCacheEntryOptions()
            {
                AbsoluteExpiration = _expirationTime,
                Priority = CacheItemPriority.High
            };

            //we need an expiration token to explicity clear the cache
            options.AddExpirationToken(new CancellationChangeToken(_clearCacheToken.Token));

            options.RegisterPostEvictionCallback((key, value, reason, substate) =>
            {
                //Logger.Error(LoggingEvents.CacheEvicted + ":: Cache item {key} was evicted because: {reason} :: Key: " +  key + " :: Reason: " + reason);
                Logger.Info(LoggingEvents.CacheEvicted + ":: Cache item {key} was evicted because: {reason} :: Key: " + key + " :: Reason: " + reason);
            });

            return options;
        }

        public void Clear()
        {
            try
            {
                if (_clearCacheToken != null && !_clearCacheToken.IsCancellationRequested && _clearCacheToken.Token.CanBeCanceled)
                {
                    _clearCacheToken.Cancel();
                    _clearCacheToken.Dispose();
                }

                _clearCacheToken = new CancellationTokenSource();

                Logger.Error(LoggingEvents.CacheClearFailed + ":: Cache cleared explicitly with /cache/clear route.");
            }
            catch (Exception ex)
            {
                Logger.Error(LoggingEvents.CacheClearFailed + ":: Exception: " + ex.Message);
            }
        }

        public List<ProviderDTO> GetProviders(bool cloneList = true)
        {
            if (!_cache.TryGetValue<List<ProviderDTO>>(nameof(GetProviders), out List<ProviderDTO> providerDTOs))
            {
                //The mapper function for providers requires the cached child collections
                //as well, since it builds a complex object model.
                var providers = this._dataService.GetAllProviders();

                var hospitalDTOs = GetHospitals();
                ////Calls private method to prevent circular reference between GetProviders and GetSpecialties.
                var specialtyDTOs = GetAllSpecialties();
                var languageDTOs = GetLanguages();
                var addressDTOs = GetAddresses();
                providerDTOs = providers.Select(p => _mapper.Map(p, hospitalDTOs, specialtyDTOs, languageDTOs, addressDTOs)).ToList();

                _cache.Set(nameof(GetProviders), providerDTOs, this.BuildCacheOptions());
            }

            //Clone the provider list so that each request can modify the returned data
            //without modifying the shared cache and affecting other users' results.
            if (cloneList)
            {
                providerDTOs = providerDTOs.Select(p => DeepClone(p)).ToList();
            }

            return providerDTOs;
        }

        public ProviderDTO GetProvider(int providerId)
        {
            var providers = GetProviders(false);
            var provider = providers.FirstOrDefault(p => p.Id == providerId);

            if (provider == null || provider.Addresses.Count != 0) 
                return provider;

            var address = new AddressDTO
            {
                Address1 = provider.Address1,
                Address2 = provider.Address2,
                Address3 = provider.Address3,
                City = provider.City,
                State = provider.State,
                Zip = provider.ZipCode,
                Phone1 = provider.Phone1,
                MainPhone = provider.MainPhone,
                Fax = provider.Fax,
                Lat = provider.Lat,
                Lng = provider.Lng
            };
            if (!provider.Addresses.Contains(address))
                provider.Addresses.Add(address);
            return provider;
        }

        public List<HospitalDTO> GetHospitals()
        {
            if (_cache.TryGetValue<List<HospitalDTO>>(nameof(GetHospitals), out var hospitalDTOs))
                return hospitalDTOs;

            var hospitals = this._dataService.GetAllHospitals();
            hospitalDTOs = hospitals.Select(h => _mapper.Map(h)).ToList();

            _cache.Set(nameof(GetHospitals), hospitalDTOs, this.BuildCacheOptions());

            return hospitalDTOs;
        }

        private List<SpecialtyDTO> GetAllSpecialties()
        {
            if (_cache.TryGetValue<List<SpecialtyDTO>>(nameof(GetAllSpecialties), out var specialtyDTOs))
                return specialtyDTOs;

            var specialties = this._dataService.GetAllSpecialties();
            specialtyDTOs = specialties.Select(s => _mapper.Map(s)).ToList();
            specialtyDTOs = specialtyDTOs.OrderBy(s => s.Text).ToList();

            _cache.Set(nameof(GetAllSpecialties), specialtyDTOs, this.BuildCacheOptions());

            return specialtyDTOs;
        }

        public List<SpecialtyDTO> GetSpecialties()
        {
            var specialtyDTOs = GetAllSpecialties();

            return specialtyDTOs;
        }

        public List<LanguageDTO> GetLanguages()
        {
            if (_cache.TryGetValue<List<LanguageDTO>>(nameof(GetLanguages), out var languageDTOs)) 
                return languageDTOs;

            var languages = this._dataService.GetAllLanguages();
            languageDTOs = languages.Select(l => _mapper.Map(l)).ToList();
            languageDTOs = languageDTOs.OrderBy(l => l.Text).ToList();

            _cache.Set(nameof(GetLanguages), languageDTOs, this.BuildCacheOptions());

            return languageDTOs;
        }

        public List<AddressDTO> GetAddresses()
        {
            if (_cache.TryGetValue<List<AddressDTO>>(nameof(GetAddresses), out var addressDTOs)) 
                return addressDTOs;

            var addresses = this._dataService.GetAllAddresses();
            addressDTOs = addresses.Select(a => _mapper.Map(a)).ToList();
            _cache.Set(nameof(GetAddresses), addressDTOs, this.BuildCacheOptions());

            return addressDTOs;
        }

        private ZipCodeInfo GetDefaultZipCodeInfo()
        {
            return new ZipCodeInfo()
            {
                IsDefault = true,
                ZipCode = 32207,
                City = "Jacksonville,",
                State = "FL",
                Lat = 30.316250,
                Long = -81.662773
            };
        }

        /// <summary>
        /// Gets the city, state and geo coordinates of a zip code.
        /// This is not called for every provider (they already have that data).
        /// It called once per search with the zip code entered into the "Zip Code" UI field.
        /// If nothing is entered, it will default to the Jacksonville zip code.
        /// </summary>
        /// <param name="zipCode"></param>
        public ZipCodeInfo GetZipCodeInfo(string zipCode)
        {
            double zipCodeNumeric;

            //Validate zip code and convert to numeric (data type used in database)
            //or default to the Jacksonville zip code
            if (zipCode == null || zipCode.Trim().Length < 5)
            {
                return GetDefaultZipCodeInfo();
            }
            else
            {
                var zipCode5 = zipCode.Trim().Substring(0, 5);
                if (!double.TryParse(zipCode5, out zipCodeNumeric))
                {
                    return GetDefaultZipCodeInfo();
                }
            }

            //Get the zip code info from cache, or from the database.
            var cacheItemName = $"{nameof(GetZipCodeInfo)}-{zipCodeNumeric}";
            if (_cache.TryGetValue<ZipCodeInfo>(cacheItemName, out ZipCodeInfo info))
                return info ?? GetDefaultZipCodeInfo();

            info = _dataService.GetZipCodeInfo(zipCodeNumeric);
            if (info != null)
            {
                _cache.Set(cacheItemName, info); //no expiration since this data does not change
            }

            return info ?? GetDefaultZipCodeInfo();
        }

        private static T DeepClone<T>(T obj)
        {
            using var ms = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, obj);
            ms.Position = 0;

            return (T)formatter.Deserialize(ms);
        }
    }
}
