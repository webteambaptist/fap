﻿using AutoMapper;
using FAP.DTOs;
using FAP.Models;

namespace FAP
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Provider, ProviderDTO>();
            CreateMap<Hospital, HospitalDTO>();
            CreateMap<Specialty, SpecialtyDTO>();
            CreateMap<Language, LanguageDTO>();
            CreateMap<Address, AddressDTO>();
            //CreateMap<InsuranceGroup, InsuranceGroupDTO>();
        }
    }
}
