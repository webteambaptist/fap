﻿using System;

namespace FAP.DTOs
{
    [Serializable]
    public class SpecialtyDTO
    {
        public string Code { get; set; }
        public string Text { get; set; }
    }
}
