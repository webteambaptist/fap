﻿using System;
using System.Collections.Generic;

namespace FAP.DTOs
{
    [Serializable]
    public class ProviderSearchDataDTO
    {
        public ProviderQueryResultDTO QueryResult { get; set; }
        public List<HospitalDTO> Hospitals { get; set; }
        public List<SpecialtyDTO> Specialties { get; set; }
        public List<LanguageDTO> Languages { get; set; }
    }
}
