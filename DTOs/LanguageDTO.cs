﻿using System;

namespace FAP.DTOs
{
    [Serializable]
    public class LanguageDTO
    {
        public string Code { get; set; }
        public string Text { get; set; }
    }
}
