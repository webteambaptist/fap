﻿using System.Collections.Generic;

namespace FAP.DTOs
{
    public class ProviderQueryResultDTO
    {
        public int TotalCount { get; set; }
        public bool IsPreferred { get; set; }
        public List<ProviderDTO> Providers { get; set; }
    }
}
