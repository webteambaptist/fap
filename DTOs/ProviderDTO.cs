﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FAP.DTOs
{
    [Serializable]
    public class ProviderDTO
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                string fullName;

                if (MiddleName != null && MiddleName.Trim().Length > 0)
                    fullName = $"{FirstName} {MiddleName[0]}. {LastName}";
                else
                    fullName = $"{FirstName} {LastName}";

                if (EchoSuffix != null && EchoSuffix.Trim().Length > 0)
                {
                    fullName += $", {EchoSuffix}";
                }

                return fullName;
            }
        }
        public string FormattedCommonName { get; set; }
        public string EchoDoctorNumber { get; set; }
        public string GenderId { get; set; }
        public string Gender
        {
            get
            {
                var code = (GenderId ?? "").Trim().ToUpper();
                return code switch
                {
                    "M" => "Male",
                    "F" => "Female",
                    "" => "",
                    _ => ""
                };
            }
        }
        public string DoctorImage { get; set; }
        public string EchoSuffix { get; set; }
        public bool IsAcceptingNewPatients { get; set; }
        public bool IsAcceptingNewPatientsVisible { get; set; }
        public string FormattedJobTitle { get; set; }
        public string LegalPracticeName { get; set; }
        public bool? IsBPP { get; set; }

        public List<HospitalDTO> Hospitals { get; set; } = new List<HospitalDTO>();
        public List<SpecialtyDTO> Specialties { get; set; } = new List<SpecialtyDTO>();
        public List<LanguageDTO> Languages { get; set; } = new List<LanguageDTO>();
        public List<AddressDTO> Addresses { get; set; } = new List<AddressDTO>();
        public List<string> AgesTreated { get; set; } = new List<string>();

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public string Phone1 { get; set; }
        public string MainPhone { get; set; }
        public string Fax { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }

        public string FullAddressLine1
        {
            get
            {
                if (Address3 != null && Address3.Trim().Length > 0)
                {
                    return $"{Address1} {Address2} {Address3}";
                }

                return $"{Address1} {Address2}";
            }
        }

        public string FullAddressLine2 => $"{City}, {State} {ZipCode}";

        public string LanguageListText
        {
            get
            {
                var lang = "";
                foreach (var l in Languages.Where(l => l.Text != null && l.Text.Trim().Length > 0))
                {
                    if (lang.Length > 0)
                    {
                        lang += ", ";
                    }
                    lang += l.Text.Trim();
                }
                return lang;
            }
        }
        public int SearchIndex { get; set; }
        public string SearchFromCity { get; set; }
        public string SearchFromState { get; set; }
        public double SearchDistance { get; set; }
    }
}
