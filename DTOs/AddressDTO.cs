﻿using System;

namespace FAP.DTOs
{
    [Serializable]
    public class AddressDTO
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Region { get; set; }
        public int AddressType { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string MainPhone { get; set; }
        public string Fax { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }

        public string FullAddressLine1
        {
            get
            {
                if (Address3 != null && Address3.Trim().Length > 0)
                {
                    return $"{Address1} {Address2} {Address3}";
                }
                else
                {
                    return $"{Address1} {Address2}";
                }
            }
        }

        public string FullAddressLine2
        {
            get
            {
                return $"{City}, {State} {Zip}";
            }
        }
    }
}
