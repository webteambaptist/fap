﻿using System;

namespace FAP.DTOs
{
    /// <summary>
    /// Query parameters for provider list including paging, sorting and filtering.
    /// </summary>
    [Serializable]
    public class SearchQueryDTO
    {
        public bool IsPreferred { get; set; }
        public int? Page { get; set; }
        public int? PageCount { get; set; }
        /// <summary>
        /// "distance" or "name"
        /// </summary>
        public string Sort { get; set; }
        /// <summary>
        /// Last name of the provider
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Baptist Physician Partner
        /// </summary>
        public bool IsBPP { get; set; }
        /// <summary>
        /// "M" or "F"
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// Search within zip code
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// Search within x miles
        /// </summary>
        public int? Distance { get; set; }
        /// <summary>
        /// Hospital codes
        /// </summary>
        public string[] Hospitals { get; set; }
        /// <summary>
        /// Specialty codes
        /// </summary>
        public string[] Specialties { get; set; }
        /// <summary>
        /// Language codes
        /// </summary>
        public string[] Languages { get; set; }
    }
}
