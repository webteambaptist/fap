﻿using System;

namespace FAP.DTOs
{
    [Serializable]
    public class InsuranceGroupDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public string Href { get; set; }
    }
}
