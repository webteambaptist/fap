﻿using System;

namespace FAP.DTOs
{
    [Serializable]
    public class HospitalDTO
    {
        public string Code { get; set; }
        public string Text { get; set; }

        public string Abbrev { get; set; }

    }
}
