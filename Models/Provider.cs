﻿using System;
using System.Collections.Generic;

namespace FAP.Models
{
    public class Provider
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FormattedCommonName { get; set; }
        public string FormattedLastNameFirst { get; set; }
        public string EchoPhysicianId { get; set; }
        public string EchoDoctorNumber { get; set; }
        public string NationalId { get; set; }
        public string ContactPhone { get; set; }
        public string ContactCellPhone { get; set; }
        public string ContactPager { get; set; }
        public string GenderId { get; set; }
        public string DoctorImage { get; set; }
        public string DoctorHeaderImage { get; set; }
        public string ThumbnailPhoto { get; set; }
        public string EchoSuffix { get; set; }
        public string UrlRoute { get; set; }
        public string FormattedUrlRoute { get; set; }
        public bool IsAcceptingNewPatients { get; set; }
        public bool IsAcceptingNewPatientsVisible { get; set; }
        public int TypeId { get; set; }
        public string Introduction { get; set; }
        public string FormattedSpecialties { get; set; }
        public bool IsArchived { get; set; }
        public bool IsHide { get; set; }
        public string NameOverride { get; set; }
        public string JobTitleOverride { get; set; }
        public string FormattedJobTitle { get; set; }
        public string LegalPracticeName { get; set; }
        public bool DoesAcceptsEapps { get; set; }
        public bool IsPCMH { get; set; }
        public bool IsBloodless { get; set; }
        public bool AdminApproved { get; set; }
        public bool IsNewDoctor { get; set; }
        public bool IsBPP { get; set; }
        public bool IsEmployed { get; set; }
        public bool DoesHavePatientReviews { get; set; }
        public string PublicationQuery { get; set; }
        public string LastUpdateDateTime { get; set; }
        public string LastUpdateUser { get; set; }
        public _Type Type { get; set; }
        public List<_GroupsMapping> GroupsMapping { get; set; }
        public List<_SpecialtyMapping> SpecialtyMapping { get; set; }
        public List<_LanguageMapping> LanguageMapping { get; set; }
        public List<_EducationMapping> EducationMapping { get; set; }
        public List<_SuffixMapping> SuffixMapping { get; set; }
        public List<_HospitalMapping> HospitalMapping { get; set; }
        public List<_AgesTreatedMapping> AgesTreatedMappings { get; set; }
        public List<_PatientFormsMapping> PatientFormsMappings { get; set; }
        public List<Locations> Locations { get; set; }
        public List<_BoardMapping> BoardMapping { get; set; }
        public List<_LicenseMapping> LicenseMapping { get; set; }
        public List<_Expertise> Expertise { get; set; }
        public List<_ProviderParagraphs> ProviderParagraphs { get; set; }
    }

    public partial class _Type
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Text { get; set; }
    }
    public partial class _GroupsMapping
    {
        public string Code { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public int OrderId { get; set; }
        public bool IsPrimary { get; set; }
    }
    public partial class _SpecialtyMapping
    {
        public string SpecialtyCode { get; set; }
        public string Text { get; set; }
        public string JobTitle { get; set; }
        public string SpecialtyStatus { get; set; }
        public string SpecialtyType { get; set; }
        public string HippaTaxonomy { get; set; }
        public bool IsBoardCertified { get; set; }
        public bool IsPrimary { get; set; }
    }
    public partial class _LanguageMapping
    {
        public string LanguageCode { get; set; }
        public string Text { get; set; }
    }
    public partial class _EducationMapping
    {
        public string DegreeCode { get; set; }
        public string DegreeText { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramText { get; set; }
        public string InsitutionCode { get; set; }
        public string InstitutionText { get; set; }
        public string InstitutionCity { get; set; }
        public string InstitutionState { get; set; }
        public string Graduate_Complete { get; set; }
        public string StartYear { get; set; }
        public string EndYear { get; set; }
        public string SequenceId { get; set; }
    }
    public partial class _SuffixMapping
    {
        public string SuffixCode { get; set; }
        public int OrderId { get; set; }
    }
    public partial class _HospitalMapping
    {
        public string HospitalCode { get; set; }
        public string HospitalName { get; set; }
        public string StaffCode { get; set; }
        public string OrderId { get; set; }
    }
    public partial class _AgesTreatedMapping
    {
        public string Text { get; set; }
        public int OrderId { get; set; }
    }
    public partial class _PatientFormsMapping
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public int OrderId { get; set; }
    }
    public partial class Locations
    {
        public int LocationId { get; set; }
        public string LocationGuid { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public bool IsAcceptingEAppointments { get; set; }
        public bool IsDefaultLocation { get; set; }
        public int SequenceId { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string type { get; set; }
        public string mainPhone { get; set; }
        public string admissionsPhone { get; set; }
        public List<_Hours> Hours { get; set; }
        public List<_Providers> OtherProviders { get; set; }
        public List<_Providers> OfficeExtenders { get; set; }
        public partial class _Hours
        {
            public string DayOfWeek { get; set; }
            public string OpenTime { get; set; }
            public string CloseTime { get; set; }
        }
        public partial class _Providers
        {
            public string CommonName { get; set; }
            public string UrlRoute { get; set; }
            public string JobTitle { get; set; }
        }
    }
    public partial class _Addresses
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
    }

    public partial class _BoardMapping
    {
        public string BoardCode { get; set; }
        public string Text { get; set; }
        public string BoardStatus { get; set; }
        public string CertificationDate { get; set; }
        public string RecertificationDate { get; set; }
        public string ExpirationDate { get; set; }
        public string CertificationNumber { get; set; }
        public string UserDef_M1 { get; set; }
        public bool Active { get; set; }
    }

    public partial class _LicenseMapping
    {
        public string LicenseType { get; set; }
        public string LicenseStatus { get; set; }
        public string State { get; set; }
        public string LicenseNumber { get; set; }
        public string AwardDate { get; set; }
        public string ExpirationDate { get; set; }
        public string Contact_Name { get; set; }
        public string Contact_Phone { get; set; }
        public string COntact_Fax { get; set; }
        public string Contact_Email { get; set; }
        public string LicensureField { get; set; }
        public string Institution_Name { get; set; }
        public string Institution_Contact { get; set; }
        public string Institution_Address1 { get; set; }
        public string Institution_Address2 { get; set; }
        public string Institution_City { get; set; }
        public string Institution_State { get; set; }
        public string Institution_Zip { get; set; }
        public bool Active { get; set; }

    }
    public partial class _Expertise
    {
        public string Text { get; set; }
        public string OrderId { get; set; }
    }
    public partial class _ProviderParagraphs
    {
        public string Text { get; set; }
        public string OrderId { get; set; }
    }


    public class FinalFAPProvider
    {
        public FapprovidersView _Fapproviders { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Fax { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string MainPhone { get; set; }
    }

    public partial class FapprovidersView
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DoctorImage { get; set; }
        public string FormattedJobTitle { get; set; }
        public string FormattedCommonName { get; set; }
        public string EchoPhysicianId { get; set; }
        public string EchoDoctorNumber { get; set; }
        public string GenderId { get; set; }
        public string EchoSuffix { get; set; }
        public bool? IsBpp { get; set; }
        public bool? IsAcceptingNewPatients { get; set; }
        public string LegalPracticeName { get; set; }
        //public string PrimarySpecialtyCode { get; set; }
        public string HospitalCodes { get; set; }
        public string SpecialtyCodes { get; set; }
        public string LanguageCodes { get; set; }
        public string OtherAddressIds { get; set; }
        public string AgesTreatedText { get; set; }
    }

}



