﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FAP.Models
{
    public partial class ZipCodeInfo
    {
        //stored in the database as a float
        public double ZipCode { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public double Lat { get; set; }

        public double Long { get; set; }

        [NotMapped]
        public bool IsDefault { get; set; }
    }
}
