﻿namespace FAP.Models
{
    public partial class Hospital
    {


        public string HospitalCode { get; set; }
        public string HospitalName { get; set; }
        public string StaffCode { get; set; }
        public string OrderId { get; set; }
    }
}
