﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FAP.Models
{
    public partial class ProviderPhoto
    {
        [Column("dr_no_ext")]
        public string DoctorNumber { get; set; }

        public byte[] Photo { get; set; }
    }
}
