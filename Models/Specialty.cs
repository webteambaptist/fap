﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAP.Models
{
    public partial class Specialty
    {
        public string Code { get; set; }
        public string Text { get; set; }
        public string DropDownValue { get; set; }
        public string JobTitle { get; set; }
        public string CertifyingBoardName { get; set; }
        public string BoardCertification { get; set; }
        public string BoardDescription { get; set; }
    }
}
