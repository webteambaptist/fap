﻿using System;

namespace FAP.Models
{
    public class InsuranceGroup
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public string Href { get; set; }
        public string LastUpdateUser { get; set; }
        public DateTime? LastUpdateDateTime { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
