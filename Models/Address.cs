﻿using System;
using System.Collections.Generic;

namespace FAP.Models
{
    public partial class Address
    {
        public int Id { get; set; }
        public int? LocationId { get; set; }
        public string ProviderGuid { get; set; }
        public string LocationGuid { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public bool IsAcceptingEappointments { get; set; }
        public bool IsDefaultLocation { get; set; }
        public int? SequenceId { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string Type { get; set; }
        public string MainPhone { get; set; }
        public string AdmissionsPhone { get; set; }
    }
}
