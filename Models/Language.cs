﻿using System;

namespace FAP.Models
{
    public partial class Language
    {
        public string Code { get; set; }
        public string Text { get; set; }
    }
}
