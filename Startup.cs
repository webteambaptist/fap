using AutoMapper;
using FAP.Config;
using FAP.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FAP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddMemoryCache();
            services.AddAutoMapper(typeof(AutoMapperProfile));

            //Database Contexts
            //services.AddDbContext<PhotosDbContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("EchoPictures")));

            //Application Services
            services.AddScoped<ICacheService, CacheService>();
            services.AddScoped<IDataService, DataService>();
            services.AddScoped<IMapperService, MapperService>();
            services.AddScoped<IQueryService, QueryService>();

            //AppSettings (Custom Configuration Settings)
            services.AddSingleton<IAppSettings, AppSettings>(
                e => Configuration.GetSection("AppSettings")
                    .Get<AppSettings>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //loggerFactory.AddFile(Configuration.GetSection("Logging"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id:int:min(1)?}");
            });
        }
    }
}
