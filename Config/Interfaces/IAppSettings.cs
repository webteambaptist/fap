﻿namespace FAP.Config
{
    public interface IAppSettings
    {
        int? CacheTimeInHours { get; set; }
        string MapApiKey { get; set; }
    }
}