﻿namespace FAP.Config
{
    public class AppSettings : IAppSettings
    {
        public int? CacheTimeInHours { get; set; }
        public string MapApiKey { get; set; }
    }
}
