﻿namespace FAP.Logging
{
    public class LoggingEvents
    {
        public const int CacheEvicted = 1001;
        public const int CacheCleared = 1002;
        public const int CacheClearFailed = 1003;

        public const int ProviderNotFound = 1100;
    }
}
