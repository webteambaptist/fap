﻿using FAP.DTOs;
using FAP.Models;
using FAP.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;
using System.Linq;
using NLog;
using System;

namespace FAP.Controllers
{
    public class HomeController : Controller
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly ICacheService _cache;
        private readonly IQueryService _queryService;

        private readonly IConfiguration _config;
        static string _privilegePortalURL;
        static string _pediatricResidentFellowPrivilegeURL;

        public HomeController(ICacheService cache, IQueryService queryService, IConfiguration config)
        {
            var logConfig = new NLog.Config.LoggingConfiguration();

            var orFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs/IntranetFAPLog-{DateTime.Now:MM-dd-yyyy}.log" };
            logConfig.AddRule(LogLevel.Info, LogLevel.Fatal, orFile);
            LogManager.Configuration = logConfig;
            
            _cache = cache;
            _queryService = queryService;
            _config = config;
            
            _privilegePortalURL = _config["AppSettings:PrivilegePortalURL"];
            _pediatricResidentFellowPrivilegeURL = _config["AppSettings:PediatricResidentFellowPrivilegeURL"];
        }

        /// <summary>
        /// Used when page initially loads. The form does not postback to this actions.
        /// Instead ajax requests post to partial view actions to maintain client-side UI state.
        /// </summary>
        [HttpGet]
        public IActionResult Index()
        {
            const bool isPreferred = false;
            return GetIndexView(isPreferred);
        }

        [HttpGet]
        [Route("preferred")]
        public IActionResult PreferredProviders()
        {
            const bool isPreferred = true;
            return GetIndexView(isPreferred);
        }

        private IActionResult GetIndexView(bool isPreferred)
        {
            var initialLoadQuery = new SearchQueryDTO()
            {
                IsPreferred = isPreferred,
                Page = 1,
                PageCount = 10,
                Sort = "name"
            };
            var model = new ProviderSearchDataDTO();
            try
            {
                model = new ProviderSearchDataDTO()
                {
                    QueryResult = _queryService.QueryProviders(initialLoadQuery),
                    Hospitals = _cache.GetHospitals(),
                    Specialties = _cache.GetSpecialties(),
                    Languages = _cache.GetLanguages()
                };
                // Filter out two hospitals
                var hospitals = model.Hospitals.Where(hospital => !hospital.Text.Equals("Baptist Emergency at Town Center") && !hospital.Text.Equals("Baptist North Medical Campus")).ToList();
                
                model.Hospitals = hospitals;

                ViewBag.PrivilegePortalURL = _privilegePortalURL;
                ViewBag.PediatricResidentFellowPrivilegeURL = _pediatricResidentFellowPrivilegeURL;

                return View("Index", model);
            }
            catch (Exception e)
            {
                Logger.Error("There was an issue loading the initial search results :: Error: " + e.Message);
            }
            return View("Index", model);
        }

        /// <summary>
        /// Provides a list of providers as HTML to the _ProviderList partial view.
        /// </summary>
        /// <param name="query"></param>
        [HttpPost]
        public IActionResult QueryProviders([FromBody]SearchQueryDTO query)
        {
            var providerQueryResult = _queryService.QueryProviders(query);
            return PartialView("_ProviderList", providerQueryResult);
        }

        [HttpGet]
        [Route("clear/cache")]
        public IActionResult ClearCache()
        {
            _cache.Clear();
            return Redirect("/");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
