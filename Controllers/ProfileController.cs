﻿using FAP.Logging;
using FAP.Services;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using FAP.DTOs;
using NLog;

namespace FAP.Controllers
{
    public class ProfileController : Controller
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        
        private readonly ICacheService _cache;
        private readonly IDataService _dataService;

        public ProfileController(ICacheService cache, IDataService dataService)
        {
            _cache = cache;
            _dataService = dataService;

            var logConfig = new NLog.Config.LoggingConfiguration();

            var orFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs/IntranetFAPLog-{DateTime.Now:MM-dd-yyyy}.log" };
            logConfig.AddRule(LogLevel.Info, LogLevel.Fatal, orFile);
            LogManager.Configuration = logConfig;
        }

        [HttpGet]
        [Route("profiles/{id:int}")]
        public IActionResult Index(int id)
        {
            var provider = _cache.GetProvider(id);

            if (provider == null)
            {
                Logger.Error(LoggingEvents.ProviderNotFound + " Provider with id {id} was not found. HTTP status code 404 was returned to the requestor. :: ID: " + id);

                return NotFound();
            }

            var doctorNumber = (provider.EchoDoctorNumber ?? "").Trim();

            if ((doctorNumber.Length <= 0 || provider.DoctorImage != "") && provider.DoctorImage != null)
            {
                Logger.Error("Provider currently does not have an image. :: DoctorNumber: " + doctorNumber + " :: Provider: " + provider.FirstName + " " + provider.LastName);
                return View(provider);
            }

            return View(provider);
        }
    }
}